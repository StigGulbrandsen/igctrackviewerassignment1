//file for all the structs
package main

import (
	"github.com/marni/goigc"
)

//Attributes for track
type Attributes struct {
	Date     string  `json:"h_date"`
	Pilot    string  `json:"pilot"`
	Glider   string  `json:"glider"`
	GliderID string  `json:"glider_id"`
	Length   float64 `json:"track_length"`
}

//the url
type _url struct {
	URL string `json:"URL"`
}

//Track identified by ID
type Track struct {
	ID    string    `json:"id"`
	Track igc.Track `json:"igc_track"`
}

//APIInfo api info struct
type APIInfo struct {
	Duration string `json:"uptime"`
	Info     string `json:"info"`
	Version  string `json:"version"`
}
