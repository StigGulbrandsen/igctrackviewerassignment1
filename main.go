package main

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func main() {
	//router
	r := mux.NewRouter()

	//handlers
	r.HandleFunc("/igcinfo", rubbishHandler)
	r.HandleFunc("/igcinfo/api", apiGet)
	r.HandleFunc("/igcinfo/api/track", igcPostGet)
	r.HandleFunc("/igcinfo/api/track/{id}", igcGetID)

	//listening to port
	err := http.ListenAndServe(":5050" /*+os.Getenv("PORT")"*/, r)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}

}
