package main

import (
	"net/http"
	"testing"

	"github.com/gorilla/mux"
)

//TestListen tests the port
func TestListen(t *testing.T) {
	r := mux.NewRouter()
	//listening to port
	err := http.ListenAndServe(":5050" /*+os.Getenv("PORT")*/, r)
	r.HandleFunc("/igcinfo", rubbishHandler)
	if err != nil {
		t.Error("Test failed")
	}

}
